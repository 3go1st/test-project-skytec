﻿using UnityEngine;
using System.Collections;

public class SphereSinusMoving : SphereMoving{

    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        TimeCounter += Time.deltaTime * MoveSpeed;
        transform.position = new Vector3(startPos.x + Mathf.Sin(TimeCounter)* amplitude,transform.position.y, transform.position.z) - (transform.up * Time.deltaTime * MoveSpeed);
    }
}
