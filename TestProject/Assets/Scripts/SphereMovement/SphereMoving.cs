﻿using UnityEngine;
using System.Collections;

public class SphereMoving : MonoBehaviour {

    public float MoveSpeed;
    public float frequency;
    public float amplitude;
    public float TimeCounter;
    public Vector3 startPos;
    public int value;


    protected GameObject GameController;


    protected void OnTriggerEnter(Collider col) {
        if (col.name != "GameOver")
        {
            Messenger<int>.Broadcast(col.name, value, MessengerMode.DONT_REQUIRE_LISTENER);
        }
        else
            Messenger.Broadcast("GameOver");

        Destroy(gameObject);
        
    }
}
