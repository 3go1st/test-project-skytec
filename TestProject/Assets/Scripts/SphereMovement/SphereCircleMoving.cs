﻿using UnityEngine;
using System.Collections;

public class SphereCircleMoving : SphereMoving {

    bool reverse;

    // Use this for initialization
    void Start()
    {
        switch (Random.Range(0, 2))
        {
            case 0: reverse = true;
                break;
            case 1: reverse = false;
                break;
        }
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        TimeCounter += Time.deltaTime * MoveSpeed;
        if (!reverse)
        {
            float x = Mathf.Cos(TimeCounter) * amplitude;
            float y = Mathf.Sin(TimeCounter) * amplitude;
            transform.localPosition = new Vector3(x, startPos.y - y - TimeCounter, startPos.z);
        }
        else if (reverse)
        {
            float x = Mathf.Cos(TimeCounter) * amplitude * -1;
            float y = Mathf.Sin(TimeCounter) * amplitude * -1;
            transform.localPosition = new Vector3(x, startPos.y - y - TimeCounter, startPos.z);
        }


    }
}
