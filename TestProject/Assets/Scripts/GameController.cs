﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour {

    [SerializeField]
    GameObject[] spheres;
    
    [SerializeField]
    float difficult;

    [SerializeField]
    float spawnTimer;

    [SerializeField]
    float difficultUpgradeTimer;

    [SerializeField]
    GameObject restartCanvas;

    public bool gameOver, difficultUpped;

    void OnEnable()
    {
        Messenger.AddListener("GameOver", GameOver);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("GameOver", GameOver);
    }

    void Start() {
            difficult = 3.0f; //по-умолчанию
            spawnTimer = 2.0f; //по-умолчанию
            Time.timeScale = 1;
            StartCoroutine(SpawnSpheres());
        
    }

    void Update()
    {
            if (Mathf.FloorToInt(Time.time) % difficultUpgradeTimer == 0 && Mathf.FloorToInt(Time.time) > 0 && !difficultUpped)
            {
                difficult++;
                difficultUpped = true;
            }
            else if (Mathf.FloorToInt(Time.time) % difficultUpgradeTimer != 0)
            {
                difficultUpped = false;
            }
     
    }

    public void GameOver() {
        gameOver = true;
        Time.timeScale = 0;
        restartCanvas.SetActive(true);
    }

    public void RestartGame() {
        SceneManager.LoadScene(Application.loadedLevel);
    }
    public void MainMenu() {
        SceneManager.LoadScene(0);
    }

    [RPC]
    IEnumerator SpawnSpheres()
    {
        while (!gameOver)
        {
            GameObject spawnedSphere;
            spawnedSphere = Instantiate(spheres[Random.Range(0, spheres.Length)], transform.position, Quaternion.identity) as GameObject;
            float amplitude = spawnedSphere.GetComponent<SphereMoving>().amplitude;
            float position = Random.Range(-11f + amplitude, 11f - amplitude);
            print(position + "     ampl = " + amplitude + "      name = " + spawnedSphere.name);
            spawnedSphere.transform.position = new Vector3(position, transform.position.y, transform.position.z);
            spawnedSphere.GetComponent<SphereMoving>().MoveSpeed = difficult;
            yield return new WaitForSeconds(spawnTimer);
        }
        yield return null;
    }
}
