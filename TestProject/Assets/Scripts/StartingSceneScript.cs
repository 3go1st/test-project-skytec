﻿using UnityEngine;
using System.Collections;

public class StartingSceneScript : MonoBehaviour {

    public void ExitGame() {
        Application.Quit();
    }

    public void GameStart() {
        Application.LoadLevel(1);
    }
}
