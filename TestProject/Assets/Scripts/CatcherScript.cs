﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
public class CatcherScript : MonoBehaviour
{

    [SerializeField]
    Text scoreText;

    int score = 0;

    void Start()
    {
    }

    void OnEnable()
    {
        Messenger<int>.AddListener(gameObject.name, ScoreAdd);
    }

    void OnDisable()
    {
        Messenger<int>.RemoveListener(gameObject.name, ScoreAdd);
    }

    void Update()
    {

        //Самый простой и примитивный скрипт движения платформы с ограничением по двум сторонам.
        if (transform.position.x > -10 && Input.GetAxis("Horizontal") < 0 || transform.position.x < 10 && Input.GetAxis("Horizontal") > 0)
        {
            transform.Translate(new Vector3(Input.GetAxis("Horizontal"), 0, 0));
        }

    }

    void ScoreAdd(int value)
    {
        score += value;
        scoreText.text = "Score: " + score;
    }
}
